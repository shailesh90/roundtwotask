<?php

namespace App;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Image extends BaseModel
{
    
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_image');
    }
    
}
