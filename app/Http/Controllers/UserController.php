<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;


class UserController extends Controller
{
    
     public function list(Request $request){

        $users = User::with('images')->paginate(1);

        return view('users.index',['users'=>$users]);
     }

}