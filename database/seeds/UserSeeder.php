<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Image;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        			[
						"user" => [
							"name" => "user1",
							"email" => "user1@a.com"
						],
						"images" => [
							"image-11-url",
							"image-12-url",
							"image-13-url",
							"image-14-url",
							"image-15-url"
						]
					],
					[
						"user" => [
							"name" => "user2",
							"email" => "user2@x.com"
						],
						"images" => [
							"image-21-url",
							"image-22-url",
							"image-23-url",
							"image-24-url",
							"image-25-url"
						]
					],
					[
						"user" => [
							"name" => "user3",
							"email" => "user3@x.com"
						],
						"images" => [
							"image-31-url",
							"image-32-url",
							"image-33-url",
							"image-34-url",
							"image-35-url"
						]
					],
					[
						"user" => [
							"name" => "user4",
							"email" => "user4@x.com"
						],
						"images" => [
							"image-41-url",
							"image-42-url",
							"image-43-url",
							"image-44-url",
							"image-45-url"
						]
					],
					[
						"user" => [
							"name" => "user5",
							"email" => "user5@x.com"
						],
						"images" => [
							"image-51-url",
							"image-52-url",
							"image-53-url",
							"image-54-url",
							"image-55-url"
						]
					],
					[
						"user" => [
							"name" => "user6",
							"email" => "user6@x.com"
						],
						"images" => [
							"image-61-url",
							"image-62-url",
							"image-63-url",
							"image-64-url",
							"image-65-url"
						]
					],
					[
						"user" => [
							"name" => "user7",
							"email" => "user7@x.com"
						],
						"images" => [
							"image-71-url",
							"image-72-url",
							"image-73-url",
							"image-74-url",
							"image-75-url"
						]
					],
					[
						"user" => [
							"name" => "user8",
							"email" => "user8@x.com"
						],
						"images" => [
							"image-81-url",
							"image-82-url",
							"image-83-url",
							"image-84-url",
							"image-85-url"
						]
					],
					[
						"user" => [
							"name" => "user9",
							"email" => "user9@x.com"
						],
						"images" => [
							"image-91-url",
							"image-92-url",
							"image-93-url",
							"image-94-url",
							"image-95-url"
						]
					],
					[
						"user" => [
							"name" => "user10",
							"email" => "user10@x.com"
						],
						"images" => [
							"image-101-url",
							"image-102-url",
							"image-103-url",
							"image-104-url",
							"image-105-url"
						]
					],
			];


			foreach ($users as $key => $user) {
				$userObj = User::create([
					'name'=> $user['user']['name'],
					'email'=> $user['user']['email'],
					'password' => Hash::make(12345678)
					]);

				$img = [];
				foreach ($user['images'] as $value) {
					$imgObj = Image::create([
						'name'=> $value
						]);
					$img[] = $imgObj->id;
				}
				$userObj->images()->sync($img);
			}

        
    }
}
