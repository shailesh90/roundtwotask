<?php

namespace App\Repositories;

/**
* Base Repository Class
*/
abstract class Repository
{
    /**
     * Model Class used for this repository
     *
     * @var null
     **/
    protected $model;

    /**
     * Intialize base classess
     *
     * @return void
     * @author Mandeep Singh <im@msingh.me>
     **/
    public function __construct()
    {
        if (!is_null($this->modelName)) {
            $class =  '\App\\' . $this->modelName;
            $this->model = new $class;
        }
        $this->auth = auth();
    }

    /**
     * Call directally model methods if it does not exists here.
     *
     * @return mixed
     * @author Mandeep Singh <im@msingh.me>
     **/
    public function __call($method, $args)
    {
        if (!is_null($this->model)) {
            return call_user_func_array([$this->model, $method], $args);
        }
        throw new Exception("No model found!", 404);
    }
}
